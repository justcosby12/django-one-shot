from django.shortcuts import get_object_or_404, redirect, render
from todos.models import TodoItem, TodoList
from todos.forms import TodoItemForm, TodoListForm

# Create your views here.
def todo_list_list(request):
    Todo = TodoList.objects.all()
    context = {
        "todo_list_list": Todo
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": detail,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
        # If all goes well, we can redirect the browser
        #   to another page and leave the function
            return redirect("todo_list_detail", id=list.id)
    else:
        # Create an instance of the Django model form class
        form = TodoListForm()
    # Put the form in the context
    context = {
        "form" : form,
    }
    # Render the HTML template with the form
    return render(request, "todos/create.html", context)

def todo_list_edit(request, pk):
    post = get_object_or_404(TodoList, id = pk)
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
        # If all goes well, we can redirect the browser
        #   to another page and leave the function
            return redirect("todo_list_detail", id=pk)
    else:
        # Create an instance of the Django model form class
        form = TodoListForm( instance=post)
    # Put the form in the context
    context = {
        "form" : form,
        "todo" : TodoList
    }
    # Render the HTML template with the form
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form" : form,
    }
    return render(request, "todos/create_item.html", context)

def todo_item_update(request, id):
    post = get_object_or_404(TodoList, id = id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "form" : form,
        "todo_item" : TodoItem
    }
    return render(request, "todos/update_item.html", context)
